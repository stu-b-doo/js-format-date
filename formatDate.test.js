const formatDate = require('./formatDate');

test('%YY/%MM/%DD', () => {
  expect(formatDate(new Date('1991-07-01T00:54:01'), "%YY/%MM/%DD")).toBe('91/07/01');
});
